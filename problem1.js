// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:

function findById(id, inventory) {
 
  if (!Array.isArray(inventory)) {
    console.log("Error: Inventory is not an array.");
    return null;
  }


  if (typeof id !== 'number') {
    console.log("Error: ID must be a number.");
    return null;
  }

 
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].id === id) {
      return inventory[index];
    }
  }

  console.log("Item with ID", id, "not found in inventory.");
  return null;
}

module.exports = findById;


