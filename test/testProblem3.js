// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
const inventory = require("../inventoryData");
const sortCarModelsAlphabetically = require("../problem3");


  const sortedCarModels = sortCarModelsAlphabetically(inventory);
  if (sortCarModelsAlphabetically != null) {
    console.log(sortedCarModels);
  } else {
    console.log("data not found");
  }

