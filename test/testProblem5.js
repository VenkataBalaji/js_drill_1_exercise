const inventory = require("../inventoryData");
const getOldCars = require("../problem5");


  let { olderCars, olderCarsCount } = getOldCars(inventory);

  console.log("Older Cars:", olderCars);
  console.log("Number of Older Cars:", olderCarsCount);
