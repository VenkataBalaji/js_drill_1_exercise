const getLastCar = require("../problem2");
const inventory = require("../inventoryData");


  let lastCar = getLastCar(inventory);
  if (lastCar) {
    console.log(`last car is a ${lastCar.car_make} ${lastCar.car_model}`);
  } else {
    console.log("Inventory is empty");
  }
