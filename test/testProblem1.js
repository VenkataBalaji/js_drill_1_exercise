// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

const findById = require("../problem1.js");
const inventory = require("../inventoryData.js");

  const car33 = findById(33, inventory);

  if (car33 !== null) {
    console.log(
      `Car ${car33.id} is a ${car33.car_year} ${car33.car_make} ${car33.car_model}`
    );
  } else {
    console.log("Car not found");
  }

