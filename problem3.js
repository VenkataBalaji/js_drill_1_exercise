// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortCarModelsAlphabetically(inventory) {
  // Check if inventory is not an array
  if (!Array.isArray(inventory)) {
    console.error("Error: Inventory must be an array.");
    return [];
  }

  // Check if inventory is empty
  if (inventory.length === 0) {
    console.log("Error: Inventory is empty.");
    return [];
  }

  let sortedCarModels = [];
  for (let outerIndex = 0; outerIndex < inventory.length - 1; outerIndex++) {
    for (
      let innerIndex = outerIndex + 1;
      innerIndex < inventory.length;
      innerIndex++
    ) {
      if (inventory[outerIndex].car_model > inventory[innerIndex].car_model) {
        let temp = inventory[outerIndex].car_model;
        inventory[outerIndex].car_model = inventory[innerIndex].car_model;
        inventory[innerIndex].car_model = temp;
      }
    }
    sortedCarModels.push(inventory[outerIndex].car_model);
  }

  return sortedCarModels;
}

module.exports = sortCarModelsAlphabetically;
