// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const getBMWAndAudiCars = (inventory) => {
  // Check if inventory is not an array
  if (!Array.isArray(inventory)) {
    console.log("Error: Inventory must be an array.");
    return [];
  }

  let BMWAndAudiCars = [];
  for (let index = 0; index < inventory.length; index++) {
    if (!inventory[index].hasOwnProperty("car_make")) {
      console.log(`Error: Missing car_make property at index ${index}.`);
    }

    if (
      inventory[index].car_make === "BMW" ||
      inventory[index].car_make === "Audi"
    ) {
      BMWAndAudiCars.push(inventory[index]);
    }
  }
  return BMWAndAudiCars;
};

module.exports = getBMWAndAudiCars;
