// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function getCarYears(inventory) {
  // Check if inventory is not an array
  if (!Array.isArray(inventory)) {
    console.log("Error: Inventory must be an array.");
    return [];
  }

  let carYears = [];
  for (let index = 0; index < inventory.length; index++) {
    // Check if each item in inventory has the car_year property
    if (!inventory[index].hasOwnProperty("car_year")) {
      console.log(`Error: Missing car_year property at index ${index}.`);
    
    }
    carYears.push(inventory[index].car_year);
  }
  return carYears;
}

module.exports = getCarYears;

