// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*";

function getLastCar(inventory) {
  // Check if inventory is not an array
  if (!Array.isArray(inventory)) {
    console.log("Error: In Suffient Data.");
    return null;
  }

  // Check if inventory is empty
  if (inventory.length === 0) {
    console.log("Error: Inventory is empty.");
    return null;
  }

  // Retrieve the last car from the inventory
  let lastCarIndex = inventory.length - 1;
  let lastCar = inventory[lastCarIndex];
  return lastCar;
}

module.exports = getLastCar;
