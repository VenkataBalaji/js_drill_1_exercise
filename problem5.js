// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const getOldCars = (inventory) => {
  // Check if inventory is not an array
  if (!Array.isArray(inventory)) {
    console.error("Error: Inventory must be an array.");
    return { olderCars: [], olderCarsCount: 0 };
  }

  let olderCars = [];
  let olderCarsCount = 0;
  for (let index = 0; index < inventory.length; index++) {
    // Check if each item in inventory has the car_year property
    if (!inventory[index].hasOwnProperty("car_year")) {
      console.error(`Error: Missing car_year property at index ${index}.`);
    }

    if (inventory[index].car_year < 2000) {
      olderCars.push(inventory[index]);
      olderCarsCount++;
    }
  }
  return { olderCars, olderCarsCount };
};

module.exports = getOldCars;

